import streamlit as st
import numpy as np
import pandas as pd
from validateResponse import *
import xlsxwriter


st.set_page_config(layout="wide")
uploaded_file = st.file_uploader("Choose an Excel File")
# error = []
num_error = 0
dictionary = {}
responseDev = []
option = st.selectbox(
    'Type of response file?',
    ('VSS', 'VFVA'))
if uploaded_file is not None:
    process_state = st.text("Processing file.....")
    num_error, dictionary = check_excel_file(uploaded_file, option)
    process_state.text("Done.")

    st.metric("Total errors", num_error)
    responseDev = export_responseDev(uploaded_file, option)
    with open("responseForDev.txt", 'w') as output:
        for row in responseDev:
            output.write(str(row) + '\n')
    with open('responseForDev.txt') as f:
        st.download_button('Download ResponseDev.txt', f)  # Defaults to 'text/plain'

if dictionary :
    df = pd.DataFrame(dictionary)
    st.table(df)
    df.to_excel("outputError.xlsx", engine='xlsxwriter')
    with open('outputError.xlsx', 'rb') as f:
        st.download_button('Download Error File', data=f, file_name = 'filename.xlsx', mime = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet')  # Defaults to 'text/plain'



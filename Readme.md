## Step 1: Install requirements
pip install -r requirements.txt
## Step 2: Run Streamlit
streamlit run server.py
## Step 3: Docker build
docker build -t responsevalidate .
## Step 4: Run Docker Container
docker run --name responsevalidate -p 8505:8505 responsevalidate
docker run --restart=always --name responsevalidate -p 8505:8505 responsevalidate
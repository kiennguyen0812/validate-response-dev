from urllib import response
import pandas as pd
import regex as re
import numpy as np
import ast
from collections import defaultdict
import openpyxl

# index column
response_id_index = 2
response123_index = [5,6,7]
response_state_index = [10,11]
tts_priority_index = 12
value_tts_index = 9
response_mode_index = [13,14,15]
dict_response_id = defaultdict(int)

def checkValidStartStop(element):
    step = element[0]
    start = element[1]
    stop = element[2]
    if(start<stop and step<= stop-start):
      return True
    return False
def check_value_tts(value_tts):
  valid = True
  if ";" in value_tts:
    for element in value_tts.split(";"):
      print(element)
      try:
        element = ast.literal_eval(element)
      except:
        print("Cannot cast to list of int")
      print("Type:", type(element))
      print("Check element:",element)
      if len(element) == 3:
        valid = checkValidStartStop(element)
        if valid == False:
          return valid
  else:
    value_tts = list(value_tts)
    if len(value_tts) == 3:
      return checkValidStartStop(value_tts)
  return valid

def remove_space(response):
  temp = str(response).replace("\n","")
  temp = temp.replace("\u200b","")
  return temp

def check_duplicate_response_id(response_id):
  response_id = remove_space(response_id)
  if(response_id):
    if not dict_response_id[response_id]:
      dict_response_id[response_id] = 1
      return True
    return False
  return True

def check_response_id(response_id):
  response_id = remove_space(response_id)
  if(response_id):
    for char in str(response_id):
      if not (char.isalnum() or char in ["_","."]):
        return False
  return True

def check_response_123(df, row, index):
  response = df.iloc[row, index]
  response = str(response).strip()
  response = remove_space(response)
  for char in str(response):
      if char == "," or char == ".":
        response = response.replace(",", " , ")
        response = response.replace(".", " . ")
        response = re.sub(r"\s{2,}", " ", response)
  df.iloc[row, index] = response
  response = response.replace('"','')
  response = response.replace('(','')
  response = response.replace(')','')
  in_bracket_response = re.findall('\[\[(.*?)\]\]', response)
  if in_bracket_response:
      response = in_bracket_response[0]
      response = re.sub(r"\|\|", "", response)
  temp = re.sub(r"{{\d}}", "", str(response))
  temp = re.sub(r"{{[^)]*}}", "", temp)
  temp = re.sub(r"\+\d{2}:\d{2}", "", temp)
  temp = re.sub(r"\d{2,3}%","",temp)
  temp = re.sub(r"km/h","", temp)
  temp = re.sub(r"##","", temp)
  for char in temp:
    if(not char.isalnum() 
       and char!= "." 
       and char!="," 
       and char!=" " 
       and char!="-" 
       and char!="\'"
       and char!="?"):
      print(f"Invalid character in response {response[:150]}")
      return False
  return True

def check_response_state(response_state):
  must_be_state = ['VA_MODE_WAKEUP','VA_MODE_WAKEUP_THEN_CONTINUOUS','VA_MODE_NONE']
  if not response_state:
    return False
  if response_state not in must_be_state:
    # print("Error Response State:", response_state)
    print("Response state must be in ", must_be_state)
    return False
  return True

def check_tts_priority(tts_priority):
  must_be_priority = ["TTS_PRIORITY_OFFLINE","TTS_PRIORITY_ONLINE","TTS_ONLINE_ONLY"]
  if tts_priority == np.nan:
    return False
  if tts_priority not in must_be_priority:
    print("TTS priority must be in ", must_be_priority)
    return False
  return True

def check_response_mode(response_mode):
  must_be_response_mode = ["N","Y","T"]
  if response_mode == np.nan:
    return False
  if response_mode not in must_be_response_mode:
    print("Response mode must be in ", must_be_response_mode)
    return False
  return True

def check_excel_file(link2file, type_response: str = 'VFVA'):
    num_error = 0
    error = []
    dictionary = {"Sheet":[],"Column":[], "Row":[], "Error":[]}
    wb = openpyxl.load_workbook(link2file)
    sheet_name = wb.sheetnames
    sheet_name.remove("Summary")
    print("Check sheet name:", sheet_name)
    for sheet in sheet_name:
        if "_draft" not in sheet:
          if type_response == "VSS":
            df = pd.read_excel(link2file, sheet_name = sheet)
            print(df.head(3))
          else:
            df = pd.read_excel(link2file, index_col=0, sheet_name = sheet)
          df = df.fillna('')
          df = df.replace(to_replace =u'\xa0', value = u'') 
          num_row = df.shape[0]
          print(f'Checking sheet {sheet}......')
          for i in range(num_row):
              if df.iloc[i,response_id_index]:
                if not check_response_id(df.iloc[i,response_id_index]):
                    print(f"Error in Response ID at row {i+2} of sheet {sheet} with value {df.iloc[i,response_id_index]}")
                    num_error+=1
                    error.append(f"Error in Response ID at row {i+2} of sheet {sheet} with value {df.iloc[i,response_id_index]}")
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response ID")
                    dictionary["Row"].append(i+2)
                    dictionary["Error"].append(df.iloc[i,response_id_index])
                if not check_duplicate_response_id(df.iloc[i,response_id_index]):
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response ID")
                    dictionary["Row"].append(i+2)
                    dictionary["Error"].append(f"Duplicate Response ID {df.iloc[i,response_id_index]}")
                if not check_response_123(df, i, response123_index[0]):
                    print(f"Error in Response 1 at row {i+2} of sheet {sheet} with value {df.iloc[i,response123_index[0]]}")
                    num_error+=1
                    error.append(f"Error in Response 1 at row {i+2} of sheet {sheet} with value {df.iloc[i,response123_index[0]]}")
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response 1")
                    dictionary["Row"].append(i+2)
                    dictionary["Error"].append(df.iloc[i,response123_index[0]])
                if not check_response_123(df, i, response123_index[1]):
                    print(f"Error in Response 2 at row {i+2} of sheet {sheet} with value {df.iloc[i,response123_index[1]]}")
                    num_error+=1
                    error.append(f"Error in Response 2 at row {i+2} of sheet {sheet} with value {df.iloc[i,response123_index[1]]}")
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response 2")
                    dictionary["Row"].append(i+2)
                    dictionary["Error"].append(df.iloc[i,response123_index[1]])
                if not check_response_123(df, i, response123_index[2]):
                    print(f"Error in Response 3 at row {i+2} of sheet {sheet} with value {df.iloc[i,response123_index[2]]}")
                    num_error+=1
                    error.append(f"Error in Response 3 at row {i+2} of sheet {sheet} with value {df.iloc[i,response123_index[2]]}")
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response 3")
                    dictionary["Row"].append(i+2)
                    dictionary["Error"].append(df.iloc[i,response123_index[2]])
                if not check_response_state(df.iloc[i, response_state_index[0]]):
                    print(f"Error in Response State (Continuos OFF) at row {i+2} of sheet {sheet} with value: {df.iloc[i,response_state_index[0]]}")
                    num_error+=1
                    error.append(f"Error in Response State (Continuos OFF) at row {i+2} of sheet {sheet} with value: {df.iloc[i,response_state_index[0]]}")
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response Response State (Continuos OFF)")
                    dictionary["Row"].append(i+2)
                    must_be_state = ['VA_MODE_WAKEUP','VA_MODE_WAKEUP_THEN_CONTINUOUS','VA_MODE_NONE']
                    if df.iloc[i,response_state_index[1]]:
                      dictionary["Error"].append(f"Response State (Continuos OFF) must be in {must_be_state}")
                    else:
                      dictionary["Error"].append("Empty")
                if not check_response_state(df.iloc[i, response_state_index[1]]):
                    print(f"Error in Response State (Continuos ON) at row {i+2} of sheet {sheet} with value: {df.iloc[i,response_state_index[1]]}")
                    num_error+=1
                    error.append(f"Error in Response State (Continuos ON) at row {i+2} of sheet {sheet} with value: {df.iloc[i,response_state_index[1]]}")
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Response State (Continuos ON)")
                    dictionary["Row"].append(i+2)
                    must_be_state = ['VA_MODE_WAKEUP','VA_MODE_WAKEUP_THEN_CONTINUOUS','VA_MODE_NONE']
                    if df.iloc[i,response_state_index[1]]:
                      dictionary["Error"].append(f"Response State (Continuos ON) must be in {must_be_state}")
                    else:
                      dictionary["Error"].append("Empty")
                if not check_value_tts(df.iloc[i, value_tts_index]):
                    num_error+=1
                    dictionary["Sheet"].append(sheet)
                    dictionary["Column"].append("Value For TTS")
                    dictionary["Row"].append(i+2)
                    dictionary["Error"].append(f"Invalid value TTS: {df.iloc[i, value_tts_index]}")
                if type_response == "VFVA":
                  if not check_tts_priority(df.iloc[i, tts_priority_index]):
                      print(f"Error in TTS Priority at row {i+2} of sheet {sheet} with value {df.iloc[i,tts_priority_index]}")
                      num_error+=1
                      error.append(f"Error in TTS Priority at row {i+2} of sheet {sheet} with value {df.iloc[i,tts_priority_index]}")
                      dictionary["Sheet"].append(sheet)
                      dictionary["Column"].append("TTS Priority")
                      dictionary["Row"].append(i+2)
                      must_be_priority = ["TTS_PRIORITY_OFFLINE","TTS_PRIORITY_ONLINE","TTS_ONLINE_ONLY"]
                      if df.iloc[i,tts_priority_index]:
                        dictionary["Error"].append(f"TTS priority must be in {must_be_priority}")
                      else:
                        dictionary["Error"].append("Empty")
                  if not check_response_mode(df.iloc[i, response_mode_index[0]]):
                      num_error+=1
                      dictionary["Sheet"].append(sheet)
                      dictionary["Column"].append("Response Mode 1")
                      dictionary["Row"].append(i+2)
                      dictionary["Error"].append(f"Invalid Response Mode: {df.iloc[i, response_mode_index[0]]}")
                  if not check_response_mode(df.iloc[i, response_mode_index[1]]):
                      num_error+=1
                      dictionary["Sheet"].append(sheet)
                      dictionary["Column"].append("Response Mode 2")
                      dictionary["Row"].append(i+2)
                      dictionary["Error"].append(f"Invalid Response Mode: {df.iloc[i, response_mode_index[1]]}")
                  if not check_response_mode(df.iloc[i, response_mode_index[2]]):
                      num_error+=1
                      dictionary["Sheet"].append(sheet)
                      dictionary["Column"].append("Response Mode 3")
                      dictionary["Row"].append(i+2)
                      dictionary["Error"].append(f"Invalid Response Mode: {df.iloc[i, response_mode_index[2]]}")

    dict_response_id.clear()
    return num_error, dictionary

def export_responseDev(link2file, type_response: str):
  responseDev = []
  wb = openpyxl.load_workbook(link2file)
  sheet_name = wb.sheetnames
  sheet_name.remove("Summary")
  for sheet in sheet_name:
    if "_draft" not in sheet:
      if type_response == "VSS":
        df = pd.read_excel(link2file, sheet_name = sheet)
      else:
        df = pd.read_excel(link2file, index_col=0, sheet_name = sheet)
      df = df.fillna('')
      df = df.replace(to_replace =u'\xa0', value = u'') 
      num_row = df.shape[0]
      for i in range(num_row):
        if(df.iloc[i,response_id_index]):
          if type_response == "VFVA":
            response = str(df.iloc[i,response_id_index]) + " || " + str(df.iloc[i, response_state_index[0]]) + \
                          " || " + str(df.iloc[i, response_state_index[1]]) + " || " + str(df.iloc[i, tts_priority_index]) + \
                          " || " + str(df.iloc[i, response_mode_index[0]]) + " || " + str(df.iloc[i, response_mode_index[1]]) + " || " + str(df.iloc[i, response_mode_index[2]]) + \
                          " ## " + str(df.iloc[i,response123_index[0]]) + " ## " + str(df.iloc[i,response123_index[1]]) +\
                          " ## " + str(df.iloc[i,response123_index[2]])
            responseDev.append(response)
          elif type_response == "VSS":
            response = df.iloc[i,response_id_index] + " || " + df.iloc[i, response_state_index[0]] + \
                          " || " + df.iloc[i, response_state_index[1]] + \
                          " ## " + df.iloc[i,response123_index[0]] + " ## " + df.iloc[i,response123_index[1]] +\
                          " ## " + df.iloc[i,response123_index[2]]
            responseDev.append(response)
  return responseDev
    


      



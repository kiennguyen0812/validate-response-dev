FROM python:3.7
WORKDIR /usr/src/app

COPY requirements.txt ./
RUN apt-get update -y && apt-get install -y --no-install-recommends build-essential
RUN pip install --no-cache-dir -r requirements.txt

COPY . .

EXPOSE 8505

CMD streamlit run server.py --server.port 8505